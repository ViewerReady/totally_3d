﻿using UnityEngine;

namespace TestEnv
{
    public class PlayerController : MonoBehaviour
    {
		[SerializeField] private float _speed;
		[SerializeField] private float _flySpeed;
		[SerializeField] private float _rotationRatchet;

		private bool _readyToSnapTurn;

		private void Update()
		{
			Vector2 move = OVRInput.Get(OVRInput.Axis2D.PrimaryThumbstick);
			float up = OVRInput.Get(OVRInput.Axis1D.PrimaryIndexTrigger);
			float down = OVRInput.Get(OVRInput.Axis1D.SecondaryIndexTrigger);

			transform.position += new Vector3(move.x * _speed, (up - down) * _flySpeed, move.y * _speed);

			Vector3 euler = transform.rotation.eulerAngles;

			if (OVRInput.Get(OVRInput.Button.SecondaryThumbstickLeft))
			{
				if (_readyToSnapTurn)
				{
					euler.y -= _rotationRatchet;
					_readyToSnapTurn = false;
				}
			}
			else if (OVRInput.Get(OVRInput.Button.SecondaryThumbstickRight))
			{
				if (_readyToSnapTurn)
				{
					euler.y += _rotationRatchet;
					_readyToSnapTurn = false;
				}
			}
			else
			{
				_readyToSnapTurn = true;
			}

			transform.rotation = Quaternion.Euler(euler);
		}
	}
}