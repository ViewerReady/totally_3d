# Totally_3D

3D assets for Totally Baseball

### Movement

**Main thumbstick:** movement on XZ axis

**Secondary thumbstick:** Rotate (with 45 degree snap)

**Main/Secondary pointer trigger:** movement on Y axis
